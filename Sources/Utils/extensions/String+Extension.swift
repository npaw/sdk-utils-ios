//
//  String+Extension.swift
//  Utils
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    
    public var isEmpty: Bool {
        guard let self = self else { return true }
        return self.isEmpty
    }
    
}
