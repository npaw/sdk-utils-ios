//
//  Data+Extension.swift
//  Utils
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

extension Data {
    
    public var dictionary: [String : Any] {
        if let dict = try? JSONSerialization.jsonObject(with: self, options: .allowFragments) as? [String : Any] {
            return dict
        } else {
            Log.error("Malformed JSON, returning empty")
            return [:]
        }
    }
    
    public var json: String {
        return printJSONData(self)
    }
    
    private func printJSONData(_ data: Data) -> String {
        return String(decoding: data, as: UTF8.self)
    }
    
}
