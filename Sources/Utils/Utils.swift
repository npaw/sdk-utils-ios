//
//  Utils.swift
//  Utils
//
//  Created by Elisabet Massó on 18/5/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation
import CommonCrypto

open class Utils {
    
    /// Returns current timestamp in milliseconds.
    ///
    /// - Returns: The timestamp.
    public static func now() -> Double {
        return Date().timeIntervalSince1970 * 1000
    }
        
    /// Transfroms a string to a hash string.
    ///
    /// It uses the first 32 bytes of its sha256.
    ///
    /// - Parameter str: The string to be hashed.
    /// - Returns: The hashed string.
    public static func sha256(str: String) -> String? {
        if let stringData = str.data(using: .utf8) as NSData? {
            let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
            var hash = [UInt8](repeating: 0, count: digestLength)
            CC_SHA256(stringData.bytes, UInt32(stringData.count), &hash)
            let data = NSData(bytes: hash, length: digestLength)
            
            var bytes = [UInt8](repeating: 0, count: data.count)
            data.getBytes(&bytes, length: data.length)
            
            var hexString = ""
            for byte in bytes {
                hexString += String(format:"%02x", UInt8(byte))
            }
            
            return String(hexString.prefix(32))
        }
        return nil
    }
    
}
