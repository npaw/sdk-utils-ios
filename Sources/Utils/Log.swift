//
//  Log.swift
//  Utils
//
//  Created by Elisabet Massó on 12/4/22.
//  Copyright © 2022 NPAW. All rights reserved.
//

import Foundation

/// Enum for log levels.
public enum Level: Int {
    /// No console outputs.
    case silent = 6
    /// Console will show errors.
    case error = 5
    /// Console will show warnings.
    case warning = 4
    /// Console will show notices like lifecycle logs.
    case notice = 3
    /// Console will show debug messages like player events.
    case debug = 2
    /// Console will show verbose messages like HTTP Requests.
    case verbose = 1
}

/// Enum extension for log levels description.
extension Level: CustomStringConvertible {
    public var description: String {
        switch self {
            case .silent: return "Silent"
            case .error: return "Error"
            case .warning: return "Warning"
            case .notice: return "Notice"
            case .debug: return "Debug"
            case .verbose: return "Verbose"
        }
    }
}

/// Provides a set of convenience methods to ease the logging.
public class Log {
    
    /// Tag for logging with the `Log` class.
    private static let tag = "NPAW"
        
    /// The current log level.
    ///
    /// Only logs of this importance or higher will be shown.
    ///
    /// The default value is `Level.error`.
    public static var level = Level.error
    
    /// Generic logging method.
    ///
    /// The message is only logged if the current `level` is lower or equal than the given `level`.
    ///
    /// - Parameters:
    ///   - project: The project from its called.
    ///   - level: The log level to use for this message.
    ///   - message: The message to print.
    public static func reportLogMessage(project: AnyObject? = nil, level logLevel: Level, message: String) {
        if level.rawValue <= logLevel.rawValue {
            /// Build String
            let project = project != nil ? "\(String(describing: type(of: project!))): " : ""
            let logString = "\(timestamp()) [\(tag): \(logLevel.description)] \(project)\(message)"
            /// Log the formatted string
            print(logString)
        }
    }
    
    /// Prints the error log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - message: The message to print.
    public static func error(tag: AnyObject? = nil, _ message: String) {
        Log.reportLogMessage(project: tag, level: .error, message: message)
    }
    
    /// Prints the warning log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - message: The message to print.
    public static func warn(tag: AnyObject? = nil, _ message: String) {
        Log.reportLogMessage(project: tag, level: .warning, message: message)
    }
    
    /// Prints the notice log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - message: The message to print.
    public static func notice(tag: AnyObject? = nil, _ message: String) {
        Log.reportLogMessage(project: tag, level: .notice, message: message)
    }
    
    /// Prints the debug log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - message: The message to print.
    public static func debug(tag: AnyObject? = nil, _ message: String) {
        Log.reportLogMessage(project: tag, level: .debug, message: message)
    }
    
    /// Prints the verbose log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - message: The message to print.
    public static func verbose(tag: AnyObject? = nil, _ message: String) {
        Log.reportLogMessage(project: tag, level: .verbose, message: message)
    }
    
    /// Prints the exception log.
    ///
    /// - Parameters:
    ///   - tag: The tag to print the project class name its comming from.
    ///   - exception: The exception to log.
    public static func exception(tag: AnyObject? = nil, _ exception: NSException) {
        /// This sends exception logs with the same level as error
        let message = "Exception: \(exception.reason ?? "nil"). Stack trace: \(exception.callStackSymbols)"
        Log.error(tag: tag, message)
    }
    
    /// Generates the current timestamp.
    private static func timestamp() -> String {
        var buffer = [Int8](repeating: 0, count: 255)
        var timestamp = time(nil)
        let localTime = localtime(&timestamp)
        strftime(&buffer, buffer.count, "%Y-%m-%d %H:%M:%S%z", localTime)
        return buffer.withUnsafeBufferPointer {
            $0.withMemoryRebound(to: CChar.self) {
                String(cString: $0.baseAddress!)
            }
        }
    }
    
}
