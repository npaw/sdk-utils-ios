Pod::Spec.new do |spec|

  # Spec Metadata
  spec.name         = 'NPAWUtilsSDK'
  spec.version      = '1.0.1'
  
  spec.module_name  = 'Utils'

  spec.summary      = 'NPAW\'s utils SDK'

  spec.description  = <<-DESC
                      NPAW\'s utils SDK.
                      DESC

  spec.homepage     = 'https://bitbucket.org/npaw/sdk-utils-ios#readme'


  # Spec License
  spec.license      = { :type => 'MIT', :file => 'LICENSE.md' }


  # Author Metadata
  spec.author             = { 'NPAW' => 'support@nicepeopleatwork.com' }

  # Platform Specifics
  spec.ios.deployment_target = '11.0'
  spec.osx.deployment_target = '10.10'
  spec.tvos.deployment_target = '11.0'

  spec.swift_version = '5.3', '5.4'


  # Source Location
  spec.source       = { :git => 'https://bitbucket.org/npaw/sdk-utils-ios.git', :tag => spec.version }


  # Source Code
  spec.source_files = 'Sources/Utils/**/*'

end
