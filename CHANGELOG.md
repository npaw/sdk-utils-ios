# Changelog

## [1.0.1] - 2022-12-20
### Changed
- Podspec module name to match SPM name

## [1.0.0] - 2022-07-22
### Added
- Release version
